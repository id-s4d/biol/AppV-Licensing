##### Users to filter (uncomment to select)
# -> use the OU Filter:
#$UsersToFilter = Get-ADUser -SearchBase "OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=D,DC=ETHZ,DC=CH" -Filter "*" | Select-Object -ExpandProperty Name
# -> Or the AD Group filter
#$UsersToFilter = Get-ADGroupMember -Identity "biol-micro-all" -Recursive  | Select-Object -ExpandProperty Name

#### Sofware to query:
# use PS> Find-AppVGroup -Sofware "ProductName"     
#  to check if the string uniquely identifies the correct sw!
#
# Example: Find-AppvGroup -Software "Matlab"   -> Does not uniquely identify the PRO version (Matlab Compiler edition, etc.)
#
$Software = "Affinity", "CLC", "SPSS", "GraphPadPrism", "Imaris", "EndNote", "Matlab_20", "MNova", "Origin", "TreeSizePro", "COMSOL"


function Find-AppvGroup {
    param (
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [string]$Software
    )

    BEGIN {}

    PROCESS {
        $Filter = "GREEN-*$Software*"

        # On 12. April 2019, IAM modified all AD groups, 
        # so we filter out any groups that were modified AFTER this IAM process
        $IAMIntroductionDate = "01-May-2019" 
        
        return Get-AdGroup -Filter { name -like $Filter -and Modified -gt $IAMIntroductionDate }
    }

    END {}
}

function Get-AppvMembers {
    param (
        [Parameter(Position = 0, Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string[]]$FilterUsers,

        [Parameter(ValueFromPipeline = $true)]
        [Microsoft.ActiveDirectory.Management.ADGroup]$Group
    )

    BEGIN {
        $Groups = @() 
        # A HashSet eliminates all duplicates automatically
        $UniqueUsers = New-Object "System.Collections.Generic.HashSet[string]"
    }

    PROCESS {
        $Groups += $Group
        $Result = @($Group | Get-ADGroupMember | Select-Object -expand name -Unique | Where-Object { $_ -in $FilterUsers })
        $UniqueUsers.UnionWith([string[]]$Result)
        [pscustomobject]@{Name = $Group.Name; UniqueMemberCount = $Result.Count; Members = $Result }
    }

    END {
        [pscustomobject]@{Name = "Total combined ($($Groups.Count) Groups):"; UniqueMemberCount = $UniqueUsers.Count; Members = [string[]]$UniqueUsers }
    }
}

function Get-AppvSubscriptionCount {
    param(
        [Parameter(ValueFromPipeline = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Software, 
        [ValidateNotNullOrEmpty()]
        [string[]]$FilterUsers
    )

    BEGIN { }

    PROCESS {
        Find-AppvGroup -Software $Software | 
        Get-AppvMembers -FilterUsers $FilterUsers | 
        Add-Member -MemberType NoteProperty -Value $Software -Name "Software" -PassThru
    }

    END { }        
}

$Software | Get-AppvSubscriptionCount -FilterUsers $UsersToFilter | Format-Table Name, UniqueMemberCount, Members -GroupBy "Software" -Wrap



<#

If you want to do it for a single software:

Find-AppvGroup -Software "SPSS" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "GraphPadPrism" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "Imaris" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "EndNote" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "Matlab_20" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "MNova" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "Origin" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "Sigma" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "TreeSizePro" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "Affinity" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "CLC" | Get-AppvMembers -FilterUsers $MicroUsers
Find-AppvGroup -Software "COMSOL" | Get-AppvMembers -FilterUsers $MicroUsers
#>