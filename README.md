# Simple App-V Licensing

## How to use

1. Open the App-V Kiosk "Kiosk -> "Software->User"
2. "View" -> "Report"
3. "Group" -> "biol-micro, all"
4. Check if all licensed software (marked as RED) is selected in the PS Script on Line 13   
   1. If not, use `Find-AppVGroup -Software "SomePartOfSoftwareName"` until you find a specific string that identifies the correct SW
   2. Example: "Matlab" does not uniquely identify PRO Matlab Versions, "Matlab Compiler Runtime" is also included (which is free).
      - Use "matlab_20" to identify all PRO Versions
      - Do this for all software
5. Edit Line 3 or 5, whatever suits your environment best (per OU or per Ad-Group)
6. Run the script

## Example Output

```powershell
   Software: Affinity

Name                              UniqueMembers
----                              -------------
GREEN-std-AffinitySuite_1.x__5                1
GREEN-trm-AffinityDesigner_1.x__5             0
GREEN-trm-AffinitySuite_1.x__5               12
GREEN-wst-AffinityDesigner_1.x__5             0
GREEN-wst-AffinitySuite_1.x__5               15
Total combined (5 Groups):                   15
```

## Reference

Author: @aurels
Documentation: https://unlimited.ethz.ch/x/TB5CAQ